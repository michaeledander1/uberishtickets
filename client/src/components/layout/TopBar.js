import React from 'react'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import {withRouter} from 'react-router'
import {userId} from '../../jwt'
import {connect} from 'react-redux'
import AccountIcon from 'material-ui-icons/AccountBox'

const TopBar = (props) => {
  const { currentUser, history, user } = props
  if (!user) {
  return (
    (
      <AppBar position="absolute" style={{zIndex:10}}>
        <Toolbar>
          <Typography variant="title" color="inherit" style={{flex: 1}} onClick={() => history.push('/events')}>
            Uber(ish) Tickets
          </Typography>
          {
          !user &&
          <Button color="inherit" onClick={() => history.push('/login')}>Login</Button>
          }
          {
            !user &&
            <Button color="inherit" onClick={() => history.push('/signup')}>Sign up</Button>
          }
          {
            <Button color="inherit" onClick={() => history.push('/events')}>All Events</Button>
          }
        </Toolbar>
      </AppBar>
    )
  )}
  if (user) {
  return (
    <AppBar position="absolute" style={{zIndex:10}}>
      <Toolbar>
        <Typography variant="title" color="inherit" style={{flex: 1}} onClick={() => history.push('/events')}>
          Uber(ish) Tickets
        </Typography>
        {
          user &&
          <div>
            <Button color="inherit"><AccountIcon /> { user.firstName }</Button>
            <Button color="inherit" onClick={() => history.push('/events/create')}>Create Event</Button>
          </div>
        }
          
        {
          !user &&
          <Button color="inherit" onClick={() => history.push('/login')}>Login</Button>
        }
        {
          !user &&
          <Button color="inherit" onClick={() => history.push('/signup')}>Sign up</Button>
        }
        {
          <Button color="inherit" onClick={() => history.push('/events')}>All Events</Button>
        }
        {
          currentUser &&
          <Button color="inherit" onClick={() => history.push('/logout')}>Log out</Button>
        }
      </Toolbar>
    </AppBar>
  )}
}

const mapStateToProps = state => ({
  user: state.currentUser && state.users &&
    state.users[userId(state.currentUser.jwt)],
  currentUser: state.currentUser
})

export default withRouter(
  connect(mapStateToProps)(TopBar)
)
