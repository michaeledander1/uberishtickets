import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {getEvents} from '../../actions/events'
import {getTicket, postComment, commentCreationSuccess, 
  ticketEditSuccess, updateTicket, stopEditMode, startEditMode, stopViewTicket} from '../../actions/tickets'
import {getUsers, visitorGetUsers} from '../../actions/users'
import {userId} from '../../jwt'
import CommentForm from './CommentForm';
import CommentList from './CommentList'
import EditTicket from './EditTicket';
import './TicketDetails.css'

class TicketDetails extends PureComponent {
  componentWillMount() {
    if (this.props.event === null) this.props.getEvents()
    if (this.props.userId === null) this.props.getUsers()
    if (this.props.users === null ) this.props.visitorGetUsers()
    if (this.props.tickets.currentTicket === null) this.props.getTicket(this.props.match.params.id)
  } 

  componentWillUnmount() {
    this.props.stopViewTicket()
  }

  handleSubmit = (data) => {
    this.props.postComment(data.description, this.props.userId, this.props.match.params.id)
  }

  handleEdit = (data) => {
    this.props.updateTicket(data)
    this.props.stopEditMode()
    this.props.ticketEditSuccess()
  }

  componentDidUpdate(prevProps) {
    if (this.props.tickets.addComment !== prevProps.tickets.addComment) {
      this.props.getTicket(this.props.match.params.id)
    }
    if (this.props.tickets.edits !== prevProps.tickets.edits) {
      this.props.getTicket(this.props.match.params.id)
    }
  }

  renderRisk = (ticketRisk) => {
    if (ticketRisk < 25) return 'lowrisk-ticket'
    if (ticketRisk >= 25 && ticketRisk < 50) return 'mediumrisk-ticket'
    if (ticketRisk >= 50) return 'highrisk-ticket'
  }

  
  render() {
    const { userId, tickets, users} = this.props
    const currentTicket = tickets.currentTicket
  
    if (!currentTicket || !users )
      return 'loading'

    if (this.props.tickets.editMode) {
      return <EditTicket handleEdit={this.handleEdit}/>
    }

    else
      return (
      <div className='current-ticket-page'>
        {
          userId === currentTicket.userId &&
          <div className='edit-button'>
          <button onClick={this.props.startEditMode}>Edit Ticket</button>
          </div>
        }
        <div className='return-to-events'>
          <Link to={`/events/${currentTicket.eventId}`}>Return to other tickets</Link>
        </div>
        <div className='title-parent-event'>
          <h1>{currentTicket.title}</h1>
        </div>
        <div>
          <h2>Sold by {users[currentTicket.userId].firstName}</h2>
        </div>
        <div>
        The fraud risk is 
        <div className={this.renderRisk(currentTicket.risk)}>%{currentTicket.risk}</div>
        </div>
        <div>
          <h2>Price</h2>
          {currentTicket.price}
        </div>
        <div className='ticket-pic-description'>
          <div className='ticket-details-image'>
            <img src={currentTicket.picture} alt="currentTicket"/>
          </div>
          <div className='ticket-details-desc'>
            <h2>Description</h2>
            {currentTicket.description}
          </div>
        </div>
        <CommentForm onSubmit={this.handleSubmit} userId={this.props.userId} />
        <h2>Comments</h2>
        <CommentList comments={currentTicket.comments} users={this.props.users} getTicket={this.props}
        className='comment-list' />
      </div>  
      )
    }
  }
  
  const mapStateToProps = state => ({
    authenticated: state.currentUser !== null,
    userId: state.currentUser && userId(state.currentUser.jwt),
    users: state.users,
    tickets: state.tickets,
    events: state.events
  })
  
  const mapDispatchToProps = {
    getEvents, getUsers, getTicket, postComment, commentCreationSuccess, 
    visitorGetUsers, updateTicket, stopEditMode, startEditMode, ticketEditSuccess,
    stopViewTicket
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(TicketDetails)