import React, {PureComponent} from 'react'


export default class CommentForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
      e.preventDefault()
      this.props.onSubmit(this.state)
	}

	handleChange = (event) => {
      const {name, value} = event.target

      this.setState({
      [name]: value
      })
    }

	render() {
	  return (
        <div className="submit-ticket-form">
  		  <form onSubmit={this.handleSubmit}>
			<label>
              Comment
              <textarea maxLength='250' name="description" value={
  				this.state.description || ''
  				} onChange={ this.handleChange } />
            </label>
            <button type="submit">Comment</button>
  		  </form>
      </div>
		)
	}
}