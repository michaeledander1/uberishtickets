import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {getEvents, joinEvent, updateEvent} from '../../actions/events'
import {getUsers} from '../../actions/users'
import {userId} from '../../jwt'
import Paper from 'material-ui/Paper'
import './EventDetails.css'

class EventDetails extends PureComponent {

  componentWillMount() {
    if (this.props.authenticated) {
      if (this.props.event === null) this.props.getEvents()
      if (this.props.users === null) this.props.getUsers()
    }
  }

  // joinGame = () => this.props.joinGame(this.props.game.id)

  // makeMove = (toRow, toCell) => {
  //   const {game, updateGame} = this.props

  //   const board = game.board.map(
  //     (row, rowIndex) => row.map((cell, cellIndex) => {
  //       if (rowIndex === toRow && cellIndex === toCell) return game.turn
  //       else return cell
  //     })
  //   )
  //   updateGame(game.id, board)
  // }



  render() {
    const {event, users, authenticated, userId} = this.props

    // if (!authenticated) return (
		// 	<Redirect to="/login" />
		// )

    if (event === null) return 'Loading...'
    if (!event) return 'Not found'

    // const player = game.players.find(p => p.userId === userId)

    // const winner = game.players
    //   .filter(p => p.symbol === game.winner)
    //   .map(p => p.userId)[0]

    return (
      <div>
        <h1>{event.name}</h1>
        <div>
          <h2>Name</h2>
          {event.tickets.map(ticket => ticket.name)}
        </div>
        <div>
          <h2>Price</h2>
          {event.tickets.map(ticket => ticket.price)}
        </div>
        <div>
          <h2>Description</h2>
          {event.tickets.map(ticket => ticket.description)}
        </div>
        <div>
          {event.tickets.map(ticket =>
          <img src={ticket.pictureUrl} alt="ticket"/>)}
        </div>
      </div>
    )  
  }
}

const mapStateToProps = (state, props) => ({
  authenticated: state.currentUser !== null,
  userId: state.currentUser && userId(state.currentUser.jwt),
  event: state.events && state.events[props.match.params.id],
  users: state.users
})

const mapDispatchToProps = {
  getEvents, getUsers/*, updateEvent*/
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails)