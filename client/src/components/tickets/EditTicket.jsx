import React, {PureComponent} from 'react'


export default class EditTicket extends PureComponent {
	state = {}

	handleSubmit = (e) => {
      e.preventDefault()
      this.props.onSubmit(this.state)
	}

	handleChange = (event) => {
      const {name, value} = event.target

      this.setState({
      [name]: value
      })
    }

	render() {
	  return (
        <div className="submit-ticket-form">
  		  <form onSubmit={this.handleEdit}>
			<label>
              Price
              <input type="text" name="price" value={
  				this.state.price || ''
  				} onChange={ this.handleChange } />
            </label>
			<label>
              Description
              <textarea maxLength='100' name="description" value={
  				this.state.description || ''
  				} onChange={ this.handleChange } />
            </label>
  			<label>
              Image
  			  <input type="text" name="picture" value={
  				this.state.picture || ''
  			    } onChange={ this.handleChange } />
  			</label>
            <button type="submit">Upload Ticket</button>
  		  </form>
      </div>
		)
	}
}