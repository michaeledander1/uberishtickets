import React, {PureComponent} from 'react'


export default class AddTicketForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
      e.preventDefault()
      this.props.onSubmit(this.state)
	}

	handleChange = (event) => {
      const {name, value} = event.target

      this.setState({
      [name]: value
      })
    }

	render() {
	  return (
        <div className="submit-ticket-form">
  		  <form onSubmit={this.handleSubmit}>
			<label>
              Title
              <input type="text" name="title" value={
  				this.state.title || ''} onChange={ this.handleChange } />
            </label>
			<br/>
			<label>
              Price
              <input type="number" name="price" maxLength="7" value={
  				this.state.price || ''
  				} onChange={ this.handleChange } />
            </label>
			<br/>
			<label>
              Description
              <textarea maxLength='250' placeholder="max 250 characters" name="description" value={
  				this.state.description || ''
  				} onChange={ this.handleChange } />
            </label>
			<br/>
  			<label>
              Image
  			  <input type="url" placeholder="https://picture.com" name="picture" value={
  				this.state.picture || ''
  			    } onChange={ this.handleChange } />
  			</label>
			<br/>
            <button type="submit">Upload Ticket</button>
  		  </form>
      </div>
		)
	}
}