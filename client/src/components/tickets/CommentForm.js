import React, {PureComponent} from 'react'


export default class CommentForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
          e.preventDefault()
          this.props.onSubmit(this.state)
          this.setState({description: ''})
        
      }

	handleChange = (event) => {
      const {name, value} = event.target

      this.setState({
      [name]: value
      })
    }

	render() {
      if(this.props.userId) {
      return (
        <div className="submit-ticket-form">
          <h1>Leave a comment</h1>
          <form onSubmit={this.handleSubmit}>
            <label>
              <textarea maxLength='250' name='description' value={
                this.state.description} 
                onChange={ this.handleChange } />
            </label>
            <button type="submit">Comment</button>
          </form>
        </div>
      )
    } else return null
  }
}