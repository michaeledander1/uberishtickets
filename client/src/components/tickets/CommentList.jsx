import React from 'react'

export default function CommentsList(props) {
    return (
      <div>
        <ul>
          { props.comments && props.comments.map(comment => (
            <li key={comment.id}>{props.users[comment.userId].firstName}{ comment.description }</li>
          )) }
          { !props.comments && <li>Loading comments...</li> }
        </ul>
        {console.log(props)}
      </div>
    )
  }