import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {addTicket} from '../../actions/tickets'
import AddTicketForm from './AddTicketForm'

class AddTicket extends PureComponent {
	handleSubmit = (data) => {
		if (!this.props.userId) {
			window.alert("You must login to upload a ticket")
		}
		if (!data.title || !data.price ||
			!data.description || !data.description) {
			window.alert("You must fill in the title, price and description fields")
		}
		else {
          this.props.addedTicket(data.title, data.picture, data.price, data.description, 
		    this.props.userId, this.props.event.id )
		}
	}

	render() {
		if (this.props.addedTicket.success) return (
			this.props.stopAddNewTicket
		)

		return (
			<div>
				<h1>Add a ticket</h1>
                {
				  !this.props.userId &&
				  <div>You must <Link to={`/login`}>login</Link> to sell a ticket</div>
				}
				<AddTicketForm onSubmit={this.handleSubmit} />

				<button onClick={this.props.stopAddNewTicket}>Return to event</button>
			</div>
		)
	}
}

const mapStateToProps = function (state) {
	return {
        addedTicket: state.addedTicket,
        authenticated: state.currentUser !== null,
        users: state.users
	}
}

const mapDispatchToProps = {
    addedTicket: addTicket
  }


export default connect(mapStateToProps, mapDispatchToProps)(AddTicket)