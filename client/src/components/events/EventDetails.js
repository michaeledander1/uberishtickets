import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {getEvents} from '../../actions/events'
import {getUsers, visitorGetUsers} from '../../actions/users'
import {stopViewTicket, viewTicket, addNewTicket, stopAddNewTicket} from '../../actions/tickets'
import {userId} from '../../jwt'
import './EventDetails.css'
import AddTicket from '../tickets/AddTicket';

class EventDetails extends PureComponent {

  componentWillMount() {

      if (this.props.event === null) this.props.getEvents()
      if (this.props.userId === null) this.props.getUsers()
      if (this.props.users === null ) this.props.visitorGetUsers()
  }

  componentDidUpdate(prevProps) {
    if (this.props.tickets.addingTicket !== prevProps.tickets.addingTicket) {
      this.props.getEvents()
    }
    if (this.props.tickets.edits !== prevProps.tickets.edits) {
      this.props.getEvents()
    }
  }

  renderRisk = (ticketRisk) => {
    if (ticketRisk < 25) return 'lowrisk'
    if (ticketRisk >= 25 && ticketRisk < 50) return 'mediumrisk'
    if (ticketRisk >= 50) return 'highrisk'
  }

  render() {
    const {event} = this.props

    if (event === null) return 'Loading...'
    if (!event) return 'Not found'

    if (this.props.tickets.addingTicket) {
      return <AddTicket event={this.props.event} userId={this.props.userId} 
        stopAddNewTicket={this.props.stopAddNewTicket}/>
    }
      
    else {
      return (
        <div className='event-details-page'>
          <div className='event-name-column'>
            <h1>{event.name}</h1>
            <button type="button" onClick={this.props.addNewTicket}>Sell a ticket for this event</button>
          </div>
          <div className='columns'>
            <div className='ticket-column-name'>
              <h2>Name</h2>
              {event.tickets.map(ticket => 
              <div key={ticket.id} className='ticket-name-grid'>
              <Link to={`/tickets/${ticket.id}`} key={ticket.id}>{ticket.title}</Link></div>
              )}
            </div>
            <div className='ticket-column-price'>
              <h2>Price</h2>
              {event.tickets.map(ticket => <div key={ticket.id} className='ticket-price-grid'>
                <div className={this.renderRisk(ticket.risk)}>
                {ticket.price}</div>
                </div>)}
            </div>
            <div className='ticket-column-description'>
              <h2>Description</h2>
              {event.tickets.map(ticket => <div key={ticket.id} className='ticket-description-grid'>{ticket.description}<img src={ticket.picture} alt="ticket"/></div>)}
              
            </div>
          </div>
       </div>
      )
    }
  }
}

const mapStateToProps = (state, props) => ({
  authenticated: state.currentUser !== null,
  userId: state.currentUser && userId(state.currentUser.jwt),
  event: state.events && state.events[props.match.params.id],
  users: state.users,
  tickets: state.tickets
})

const mapDispatchToProps = {
  getEvents, getUsers, stopViewTicket, 
  viewTicket, addNewTicket, stopAddNewTicket, visitorGetUsers
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails)
