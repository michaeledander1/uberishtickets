import React, {PureComponent} from 'react'
import {getEvents/*, createEvent*/} from '../../actions/events'
import {getUsers} from '../../actions/users'
import {connect} from 'react-redux'
// import {Redirect} from 'react-router-dom'
import Button from 'material-ui/Button'
import Paper from 'material-ui/Paper'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Typography from 'material-ui/Typography'
import './EventsList.css'

class EventsList extends PureComponent {
  componentWillMount() {
    if (this.props.authenticated) {
      if (this.props.events === null) this.props.getEvents()
      if (this.props.users === null) this.props.getUsers()
    }
  }

  renderEvent = (event) => {
    const {history} = this.props

    return (<Card key={event.id} className="event-card">
      <CardContent>
        <Typography color="textSecondary">
          <img src={event.photoUrl} alt='event logo' onClick={() => history.push(`/events/${event.id}`)}/>
        </Typography>
        <Typography variant="headline" component="h2">
          {event.name}
        </Typography>
        <Typography color="textSecondary">
          Status: {event.status}
        </Typography>
      </CardContent>
    </Card>)
  }

  render() {
    const {events, users, authenticated, createGame} = this.props

    // if (!authenticated) return (
		// 	<Redirect to="/login" />
		// )

    if (events === null || users === null) return null

    return (<Paper className="outer-paper">
      <Button
        color="primary"
        variant="raised"
        // onClick={createGame}
        className="create-event"
      >
        Create Game
      </Button>

      <div>
        {events.map(event => this.renderEvent(event))}
      </div>
    </Paper>)
  }
}

const mapStateToProps = state => ({
  authenticated: state.currentUser !== null,
  users: state.users === null ? null : state.users,
  events: state.events === null ?
    null : Object.values(state.events).sort((a, b) => b.id - a.id)
})

export default connect(mapStateToProps, {getEvents, getUsers, /*createEvent*/})(EventsList)
