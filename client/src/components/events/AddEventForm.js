import React, {PureComponent} from 'react'


export default class AddEventForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
      e.preventDefault()
      this.props.onSubmit(this.state)
      this.setState({})
	}

	handleChange = (event) => {
      const {name, value} = event.target

      this.setState({
      [name]: value
      })
    }

	render() {
	  return (
        <div className="submit-event-form">
  		  <form onSubmit={this.handleSubmit}>
			<label>
              Title
              <input type="text" name="name" value={
  				this.state.name || ''} onChange={ this.handleChange } />
            </label>
            <br/>
			<label>
              Start Date
              <input type="date" name="startDate" value={
  				this.state.startDate || ''
  				} onChange={ this.handleChange } />
            </label>
            <br/>
            <label>
              End Date
              <input type="date" name="endDate" value={
  				this.state.endDate || ''
  				} onChange={ this.handleChange } />
            </label>
            <br/>
			<label>
              Description
              <textarea maxLength='100' name="description" value={
  				this.state.description || ''
  				} onChange={ this.handleChange } />
            </label>
            <br/>
  			<label>
              Image
  			  <input type="text" name="photoUrl" value={
  				this.state.photoUrl || ''
  			    } onChange={ this.handleChange } />
  			</label>
            <button type="submit">Upload Event</button>
  		  </form>
      </div>
		)
	}
}