import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {getEvents, joinEvent, updateEvent} from '../../actions/events'
import {getUsers} from '../../actions/users'
import {userId} from '../../jwt'
import Paper from 'material-ui/Paper'
import './GameDetails.css'

class EventDetails extends PureComponent {

  componentWillMount() {
    if (this.props.authenticated) {
      if (this.props.event === null) this.props.getEvents()
      if (this.props.users === null) this.props.getUsers()
    }
  }

  // joinGame = () => this.props.joinGame(this.props.game.id)

  // makeMove = (toRow, toCell) => {
  //   const {game, updateGame} = this.props

  //   const board = game.board.map(
  //     (row, rowIndex) => row.map((cell, cellIndex) => {
  //       if (rowIndex === toRow && cellIndex === toCell) return game.turn
  //       else return cell
  //     })
  //   )
  //   updateGame(game.id, board)
  // }



  render() {
    const {event, users, authenticated, userId} = this.props

    if (!authenticated) return (
			<Redirect to="/login" />
		)

    if (event === null || users === null) return 'Loading...'
    if (!event) return 'Not found'

    // const player = game.players.find(p => p.userId === userId)

    // const winner = game.players
    //   .filter(p => p.symbol === game.winner)
    //   .map(p => p.userId)[0]

    return (<Paper className="outer-paper">
        <h1>{event.name}</h1>

      {/* <p>Status: {game.status}</p>

      {
        game.status === 'started' &&
        player && player.symbol === game.turn &&
        <div>It's your turn!</div>
      }

      {
        game.status === 'pending' &&
        game.players.map(p => p.userId).indexOf(userId) === -1 &&
        <button onClick={this.joinGame}>Join Game</button>
      }

      {
        winner &&
        <p>Winner: {users[winner].firstName}</p>
      }

      <hr />

      {
        game.status !== 'pending' &&
        <Board board={game.board} makeMove={this.makeMove} />
      } */}
    </Paper>)
  }
}

const mapStateToProps = (state, props) => ({
  authenticated: state.currentUser !== null,
  userId: state.currentUser && userId(state.currentUser.jwt),
  event: state.events && state.events[props.match.params.id],
  users: state.users
})

const mapDispatchToProps = {
  getEvents, getUsers, updateEvent
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails)
