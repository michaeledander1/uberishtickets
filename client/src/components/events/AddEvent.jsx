import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {addEvent} from '../../actions/events'
import {Redirect} from 'react-router-dom'

class AddEvent extends PureComponent {
	handleSubmit = (data) => {
		if (!this.props.userId) {
			window.alert("You must login to create an Event")
		}
		else {
          this.props.addEvent(data.name, data.photoUrl, data.startDate,
            data.endDate, data.description, this.props.userId)
		}
	}

	render() {
		if (!this.props.userId) return (
            <div>You must <Link to={`/login`}>login</Link> to create an Event</div>
		)

		return (
			<div>
				<h1>Create an Event</h1>
				<AddEventForm onSubmit={this.handleSubmit} />
			</div>
		)
	}
}

const mapStateToProps = function (state) {
	return {
        authenticated: state.currentUser,
        users: state.users,
        userId: state.currentUser && userId(state.currentUser.jwt),
	}
}

const mapDispatchToProps = {
    addEvent
  }


export default connect(mapStateToProps, mapDispatchToProps)(AddTicket)