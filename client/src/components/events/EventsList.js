import React, {PureComponent} from 'react'
import {getEvents} from '../../actions/events'
import {getUsers, visitorGetUsers} from '../../actions/users'
import {connect} from 'react-redux'
import Paper from 'material-ui/Paper'
import Card, { CardContent } from 'material-ui/Card'
import Typography from 'material-ui/Typography'
import './EventsList.css'

class EventsList extends PureComponent {
  componentWillMount() {

      /*if (this.props.events === null)*/ this.props.getEvents()
      if (this.props.authenticated === null) this.props.getUsers()
      if (this.props.users === null ) this.props.visitorGetUsers()
  }

  renderEvent = (event) => {
    const { history} = this.props
    var moment = require('moment');
    const now = moment()
    if (!moment(event.endDate).isBefore(now))
    return (<Card key={event.id} className="event-card">
      <CardContent>
        <Typography color="textSecondary">
          <img src={event.photoUrl} alt='event logo' onClick={() => history.push(`/events/${event.id}`)}/>
        </Typography>
        <Typography variant="headline" component="h2">
          {event.name}
        </Typography>
      </CardContent>
    </Card>)
  }

  render() {
    const {events} = this.props
    if (events === null) return "hello"

    return (<Paper className="outer-paper">
      <div className='event-grid'>
        <div>
          {events.map(event => this.renderEvent(event))}
          <button>next</button>
        </div>
      </div>
    </Paper>)
  }
}

const mapStateToProps = state => ({
  authenticated: state.currentUser !== null,
  users: state.users === null ? null : state.users,
  events: state.events === null ?
    null : Object.values(state.events).sort((a, b) => b.id - a.id)
})

export default connect(mapStateToProps, {getEvents, getUsers, visitorGetUsers})(EventsList)
