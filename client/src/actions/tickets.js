import * as request from 'superagent'
import {baseUrl} from '../constants'
import {isExpired} from '../jwt'
import {logout} from './users'

export const UPDATE_TICKET = 'UPDATE_TICKET'
export const UPDATE_TICKETS = 'UPDATE_TICKETS'
export const TICKET_CREATION_SUCCESS = 'TICKET_CREATION_SUCCESS'
export const TICKET_CREATION_FAILED = 'TICKET_CREATION_FAILED'
export const VIEW_TICKET = 'VIEW_TICKET'
export const STOP_VIEW_TICKET = 'STOP_VIEW_TICKET'
export const ADD_NEW_TICKET = 'ADD_NEW_TICKET'
export const STOP_ADD_NEW_TICKET = 'STOP_ADD_NEW_TICKET'
export const COMMENT_CREATION_SUCCESS = 'COMMENT_CREATION_SUCCESS'
export const EDIT_MODE = 'EDIT_MODE'
export const STOP_EDIT_MODE = 'STOP_EDIT_MODE'
export const UPDATE_TICKET_SUCCESS = 'UPDATE_TICKET_SUCCESS'
export const TICKET_EDIT_SUCCESS = 'TICKET_EDIT_SUCCESS'

const ticketCreationFailed = (error) => ({
  type: TICKET_CREATION_FAILED,
  payload: error || 'Unknown error'
})

const ticketCreationSuccess = () => ({
  type: TICKET_CREATION_SUCCESS
})

export const viewTicket = (ticket) => ({
	type: VIEW_TICKET,
	payload: ticket
})

export const stopViewTicket = () => ({
	type: STOP_VIEW_TICKET,
	payload: null
})

export const addNewTicket = () => ({
	type: ADD_NEW_TICKET
})

export const startEditMode = () => ({
	type: EDIT_MODE
})
export const stopEditMode = () => ({
	type: STOP_EDIT_MODE
})

export const stopAddNewTicket = () => ({
	type: STOP_ADD_NEW_TICKET
})

export const commentCreationSuccess = () => ({
	type: COMMENT_CREATION_SUCCESS,
	payload: 1
})

export const updateTicketSuccess = () => ({
	type: UPDATE_TICKET_SUCCESS
})

export const ticketEditSuccess = () => ({
	type: TICKET_EDIT_SUCCESS,
	payload: 1
})

export const addTicket = (title, picture, price, description, userId, eventId) => (dispatch) =>
	request
		.post(`${baseUrl}/events/${eventId}`)
		.send({ title, picture, price, description, userId, eventId })
		.then(result => {
			console.log(result)
			dispatch(ticketCreationSuccess())
		})
		.catch(err => {
			if (err.status === 400) {
				console.log(err.response.body.message)
				dispatch(ticketCreationFailed(err.response.body.message))
			}
			else {
				console.error(err)
			}
		})

export const getTicket = (id) => (dispatch, getState) => {
	request
		.get(`${baseUrl}/tickets/${id}`)
		.then(result => dispatch(viewTicket(result.body)))
		.catch(err => console.error(err))
}

export const postComment = (description, userId, ticketId) => (dispatch) =>
	request
		.post(`${baseUrl}/tickets/${ticketId}`)
		.send({ description, userId, ticketId })
		.then(result => {
			dispatch(commentCreationSuccess())
		})
		.catch(err => {
			if (err.status === 400) {
				console.log(err.response.body.message)
				dispatch(ticketCreationFailed(err.response.body.message))
			}
			else {
				console.error(err)
			}
		})

export const updateTicket = (update) => (dispatch, getState) => {
	const state = getState()
	const jwt = state.currentUser.jwt
    if (isExpired(jwt)) return dispatch(logout())
	
	request
		.put(`${baseUrl}/ticket/${state.tickets.currentTicket.id}`)
		.set('Authorization', `Bearer ${jwt}`)
		.send( update )
		.then(_ => dispatch(updateTicketSuccess()))
		.catch(err => console.error(err))
	}