import * as request from 'superagent'
import {baseUrl} from '../constants'
// import {logout} from './users'
// import {isExpired} from '../jwt'

export const ADDED_EVENT = 'ADDED_EVENT'
export const UPDATE_EVENT = 'UPDATE_EVENT'
export const UPDATE_EVENTS = 'UPDATE_EVENTS'
export const JOIN_EVENT_SUCCESS = 'JOIN_EVENT_SUCCESS'
export const UPDATE_EVENT_SUCCESS = 'UPDATE_EVENT_SUCCESS'
export const EVENT_CREATION_FAILED = 'EVENT_CREATION_FAILED'

const updateEvents = events => ({
  type: UPDATE_EVENTS,
  payload: events
})

const eventCreationFailed = (error) => ({
  type: EVENT_CREATION_FAILED,
  payload: error || 'Unknown error'
})

const addedEvent = event => ({
  type: ADDED_EVENT,
  payload: 1
})

export const addEvent = (name, photoUrl, startDate, endDate, description, userId) => (dispatch) =>
  request
		.post(`${baseUrl}/events/create`)
		.send({ name, photoUrl, startDate, endDate, description, userId } )
		.then(result => {
			dispatch(addedEvent())
		})
		.catch(err => {
			if (err.status === 400) {
				console.log(err.response.body.message)
				dispatch(eventCreationFailed(err.response.body.message))
			}
			else {
				console.error(err)
			}
		})


export const getEvents = (number) => (dispatch, getState) => {
  // const state = getState()
  // if (!state.currentUser) return null
  // const jwt = state.currentUser.jwt

  // if (isExpired(jwt)) return dispatch(logout())

  request
    .get(`${baseUrl}/events`)
    // .set('Authorization', `Bearer ${jwt}`)
    .then(result => dispatch(updateEvents(result.body)))
    .catch(err => console.error(err))
}


