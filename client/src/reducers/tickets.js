import {
  ADD_NEW_TICKET, STOP_ADD_NEW_TICKET, TICKET_CREATION_SUCCESS, 
  TICKET_CREATION_FAILED, VIEW_TICKET, STOP_VIEW_TICKET,
   COMMENT_CREATION_SUCCESS, EDIT_MODE, STOP_EDIT_MODE, TICKET_EDIT_SUCCESS
} from '../actions/tickets'

export default function (state = {currentTicket: null, addingTicket: false, addComment: 0, edits: 0}, {type, payload}) {
	switch(type) {
    case TICKET_CREATION_SUCCESS:
      return {
        ...state,
        success: true,
        addingTicket: false
      }

    case TICKET_CREATION_FAILED:
      return {
        ...state,
        error: payload
      }
    case VIEW_TICKET:
      return {
        ...state,
        currentTicket: payload
      }

    case STOP_VIEW_TICKET:
      return {
        ...state,
        currentTicket: payload
      }
    
    case ADD_NEW_TICKET:
      return {
        ...state,
        addingTicket: true
      }
    
    case EDIT_MODE:
      return {
        ...state,
        editMode: true
    }

    case STOP_EDIT_MODE:
      return {
        ...state,
        editMode: false
      }
    
    case STOP_ADD_NEW_TICKET:
      return {
        ...state,
        addingTicket: false
      }
    case COMMENT_CREATION_SUCCESS:
      return {
        ...state,
        addComment: state.addComment + payload
      }
    case TICKET_EDIT_SUCCESS:
      return {
        ...state,
        edits: state.edits + payload
      }
		 default:
      return state
	}
}
