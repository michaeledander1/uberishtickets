# Uber-ish Tickets

This app displays tickets for events. It was a final assignment for the web academy I attended, and had to be completed in 3 and a half days. It uses Typescript, Koa, routing-controllers and TypeORM in the backend and React/Redux in the frontend. The backend exposes a REST API but also sends messages over websockets using SocketIO. 
