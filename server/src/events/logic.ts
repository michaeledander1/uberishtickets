
import {Ticket, Event} from './entities'
import  User  from '../users/entity'

var moment = require('moment');

export const pastEvents = (event: Event) => {
    const now = moment()
    const difference = moment(event.endDate).isBefore(now)
    return difference
}

export const totalUserTickets = (user: User): number => {
    if (user.tickets.length === 0 || null) return 10
    else return 0
}

export const priceRisk = (event: Event, ticket: Ticket): number => {
    const newTicketPrice = parseFloat(ticket.price)
    const averageFinder = (accumulator, currentValue) => accumulator + currentValue;

    const priceArray = event.tickets.map(ticket => parseFloat(ticket.price))
    const total = priceArray.reduce(averageFinder, newTicketPrice)
    const average = total / (event.tickets.length + 1)
    const difference = newTicketPrice / average
    if (difference > 1 ) return (difference * 100) 
    if (difference < 1) {
      const risk  = (1 - difference) * 100
      if (risk > 10) return -10
      else return -risk
    }
    else return 0
}

export const ticketCommentCount = (ticket: Ticket): number => {
    if (ticket.comments.length === 3) return 5
    else return 0
}

export const timeUploaded = (ticket: Ticket): number => {
    const parsedHour = moment(ticket.timestamp).local()
    const hour = parsedHour.format('HH')
    if (hour >= 9 && hour <= 17) return -10
    else return 10
}

export const average = (event: Event): number => {
    const averageFinder = (accumulator, currentValue) => accumulator + currentValue;

    const priceArray = event.tickets.map(ticket => parseFloat(ticket.price))
    const total = priceArray.reduce(averageFinder, 0)
    const average = total / (event.tickets.length)
    return average
}

export const getNewRisks = (average: number, ticketFromArray: Ticket): number => {
    const difference = parseFloat(ticketFromArray.price) / average
    if (difference > 1 ) return (difference -1)  * 100
    if (difference < 1) {
      const risk  = (1 - difference) * 100
      if (risk > 10) return -10
      else return -risk
    }
    else return 0    
}

export const calculateRisk = (ticket: Ticket, event: Event, user: User): number => {
    const userRisk = totalUserTickets(user)
    const priceRiskNumber = priceRisk(event, ticket)
    const timeRisk = timeUploaded(ticket)
    const totalRisk = Math.round(userRisk + priceRiskNumber + timeRisk)
    if (totalRisk < 5) return 5
    if (totalRisk > 95) return 95
    else return (totalRisk + 5)
}

export const calculateAllRisks = (average: number, ticket: Ticket, user: User): number => {
    const userRisk = totalUserTickets(user)
    const priceRiskNumber = getNewRisks(average, ticket)
    const timeRisk = timeUploaded(ticket)
    const totalRisk = Math.round(userRisk + priceRiskNumber + timeRisk)
    if (totalRisk <= 5) return 5
    if (totalRisk >= 95) return 95
    else return (totalRisk + 5)
} 


