import { 
  JsonController, Param, Get, Body, Post, Put, CurrentUser, NotFoundError, Authorized, ForbiddenError
} from 'routing-controllers'
import { Event, Ticket, Comment } from './entities'
import User from '../users/entity';
import { ticketCommentCount, average, calculateAllRisks } from './logic';


@JsonController()
export default class EventController {

  // @Authorized()
  @Get('/events/:id([0-9]+)')
  getEvent(
    @Param('id') id: number
  ) {
    return Event.findOneById(id)
  }

  // @Authorized()
  @Get('/events')
  async getEvents(
    // @Body() data: number,
  ) {
    // const allEvents = await Event.find()
    // if(!allEvents) throw new NotFoundError("Can't find events!")

    // const validEvents = await Promise.all(
    //   allEvents.filter(thing => {
    //   pastEvents(thing) === false
    //   }))
    return Event.find(/*{*/)
      // take: 9,
      // skip: data
    // console.log(validEvents)
    // console.log(`these are valid EVENTSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs ${validEvents}`)
    
    // return validEvents
  }
  

  @Post('/events/create')
  async addedEvent(
    @Body() data: Event,
  ) {
    console.log(`this is the data: ${data}`)
    const {userId, ...rest} = data
    const userParent = await User.findOneById(userId)
    const event = await Event.create({user: userParent, 
      photoUrl: data.photoUrl, 
      name: data.name, startDate: data.startDate, endDate: data.endDate,
      description: data.description}).save()
    return event
  }

  @Post('/events/:id([0-9]+)')
  async addtickets(
    @Body() data: Ticket,
    @Param('id') id: number
  ) {
    const {userId} = data
    const eventParent = await Event.findOneById(id)
    if (!eventParent) throw new NotFoundError(`Event does not exist`)
    
    const userParent = await User.findOneById(userId)
    if (!userParent) throw new NotFoundError(`User does not exist`)
  
    const ticket = await Ticket.create({user: userParent, 
      event: eventParent, picture: data.picture, 
      title: data.title, price: data.price,
      description: data.description}).save()

    const newTicket = await Ticket.findOneById(ticket.id)
    if (!newTicket) throw new NotFoundError(`Ticket does not exist`)

    const updatedEvent = await Event.findOneById(id)
    if (!updatedEvent) throw new NotFoundError(`Ticket does not exist`)

    const averagePrice = average(updatedEvent)
    console.log(averagePrice)
    
    const updatedTickets = await Promise.all(updatedEvent.tickets.map(item => {
        const newRisk = calculateAllRisks(averagePrice, item, userParent)
        const newTickets = Ticket.merge(item, {risk: newRisk}).save()
        return newTickets
    }))
    return newTicket
  }

  @Get('/tickets/:id([0-9]+)')
  getTicket(
    @Param('id') id: number
  ) {
    return Ticket.findOneById(id)
  }

  @Post('/tickets/:id([0-9]+)')
  async postComment(
    @Body() data: Comment,
  ) {
    const {userId, ticketId, description} = data
    const ticketParent = await Ticket.findOneById(ticketId)
    const userParent = await User.findOneById(userId)
    const comment = await Comment.create({user: userParent, 
      ticket: ticketParent, description: description}).save()
    const updatedTicket = await Ticket.findOneById(ticketId)
    if (!updatedTicket) throw new NotFoundError(`Ticket does not exist`)
      
    const newRisk = (updatedTicket.risk + ticketCommentCount(updatedTicket))
    Ticket.merge(updatedTicket, {risk: newRisk}).save()
    
    return comment
  }

  @Authorized()
  @Put('/ticket/:id([0-9]+)')
  async updateTicket(
    @CurrentUser() user: User,
    @Param('id') ticketId: number,
    @Body() update: Partial<Ticket>
  ) {
    const oldTicket = await Ticket.findOneById(ticketId)
    if (!oldTicket) throw new NotFoundError(`Ticket does not exist`)

    const currentUser: any = await User.findOneById(user.id)
    console.log(currentUser)

    if (currentUser.id !== oldTicket.userId) throw new ForbiddenError(`You can't update this ticket`)
    if (oldTicket.price === update.price)  return Ticket.merge(oldTicket, update).save()
    else {
      const updatedTicket = Ticket.merge(oldTicket, update).save()
      if (!updatedTicket) throw new NotFoundError(`Ticket does not exist`)

      const event = await Event.findOneById(oldTicket.eventId)
      if (!event) throw new NotFoundError(`Event does not exist`)
      
      const averagePrice = average(event)

      const updatedTickets = await Promise.all(event.tickets.map(item => {
        const newRisk = calculateAllRisks(averagePrice, item, currentUser)
        const newTickets = Ticket.merge(item, {risk: newRisk}).save()
        return newTickets
    }))

    return updatedTicket /*&& updatedTickets*/
    }
  }

}
