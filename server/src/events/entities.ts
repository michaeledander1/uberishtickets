import { BaseEntity, PrimaryGeneratedColumn, Column, Entity, OneToMany, ManyToOne, CreateDateColumn } from 'typeorm'
import User from '../users/entity'

@Entity()
export class Event extends BaseEntity {

  @PrimaryGeneratedColumn()
  id?: number

  @Column('text')
  name: string

  @Column('text')
  photoUrl: string

  @Column('text')
  description: string

  // @IsDate()
  @Column('text')
  startDate: string

  // @IsDate()
  @Column('text')
  endDate: string

  @ManyToOne(_ => User, user => user.events)
  user: User

  @Column()
  userId: number

  @OneToMany(_ => Ticket, ticket => ticket.event, {eager:true})
  tickets: Ticket[]
}

@Entity()
export class Ticket extends BaseEntity {

  @PrimaryGeneratedColumn()
  id?: number

  @CreateDateColumn()
  timestamp: string;

  @ManyToOne(_ => User, user => user.tickets)
  user: User

  @ManyToOne(_ => Event, event => event.tickets)
  event: Event

  @OneToMany(_ => Comment, comment => comment.ticket, {eager:true})
  comments: Comment[]

  @Column()
  userId: number

  @Column()
  eventId: number

  @Column('text')
  title: string

  @Column('text', {nullable: true})
  picture: string

  @Column('text')
  price: string

  @Column('text')
  description: string

  @Column('int', {default: 5})
  risk: number
}

@Entity()
export class Comment extends BaseEntity {

  @PrimaryGeneratedColumn()
  id?: number

  @ManyToOne(_ => User, user => user.comments)
  user: User

  @ManyToOne(_ => Ticket, ticket => ticket.comments)
  ticket: Ticket

  @Column()
  userId: number

  @Column()
  ticketId: number

  @Column('text')
  description: string
}
